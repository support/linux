add-apt-repository ppa:seafile/seafile-client
apt-get update
apt-get -y install keepass2 seafile-gui gnome-color-manager
cat > 20-intel.conf <<EOL
Section "Device"
        Identifier  "card0"
        Driver      "intel"
        Option      "Backlight"  "intel_backlight"
        BusID       "PCI:0:2:0"
EndSection
EOL
mv 20-intel.conf /usr/share/X11/xorg.conf.d/
apt-get -y remove avahi-autoipd avahi-daemon avahi-utils
